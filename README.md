Model was trained using fasterRCNN model, resnet50 configuration file was used. It was trained to detect below brands of 3 different products, i.e. cake, corn and tea.

real_cake
dr.oetker_cake
ruf_cake

real_corn
tip_corn
bonduelle_corn

tip_tea
mebmer_tea
teekanne_tea

model was tested using Object_detection_image.py python file, and output was stored in detected_images folder.


